# Blog

[![star](https://gitee.com/zhong96/FBlog/badge/star.svg?theme=dark)](https://gitee.com/zhong96/FBlog)
[![fork](https://gitee.com/zhong96/FBlog/badge/fork.svg?theme=dark)](https://gitee.com/zhong96/FBlog/members)

#### 介绍
使用SSM整合快速构建一个blog系统，包括前后台模块。

#### 软件架构
> 软件架构说明
>
> 使用Spring4+Springmvc+Mybatis3架构，
>
> 采用Mysql数据库；
>
> 使用Maven3管理项目，使用Shiro作为项目安全框架，使用Lucene作为全文检索，支持restful风格；
>
> 前台网页使用主流的Bootstrap3 UI框架；后台管理使用主流易用的EasyUI轻量级框架；
>
> 数据库连接池使用的是阿里巴巴的Druid；
>
> 在线编辑器使用了百度的UEditor，支持单图，多图上传，支持截图上传，支持代码高亮特性；


#### 安装教程

1.  git clone  https://gitee.com/zhong96/FBlog.git
2.  将下载的项目导入到IDEA中，并且下载pom中的依赖jar包
3.  配置项目运行环境，由于是使用jsp开发的，需要配置外部的tomcat服务器
4.  运行项目，在浏览器中输入 http://127.0.0.1:8080/FBlog即可访问项目

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_yourId 分支
3.  提交代码
4.  新建 Pull Request
