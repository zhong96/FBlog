package com.gqzdev.service.impl;

import com.gqzdev.dao.BlogTypeDao;
import com.gqzdev.entity.BlogType;
import com.gqzdev.service.BlogTypeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 博客类型Service实现类
 */
@Service("blogTypeService")
public class BlogTypeServiceImpl implements BlogTypeService{

	@Resource
	private BlogTypeDao blogTypeDao;

	@Override
	public List<BlogType> countList() {
		return blogTypeDao.countList();
	}

	@Override
	public List<BlogType> list(Map<String, Object> map) {
		return blogTypeDao.list(map);
	}

	@Override
	public Long getTotal(Map<String, Object> map) {
		return blogTypeDao.getTotal(map);
	}

	@Override
	public Integer add(BlogType blogType) {
		return blogTypeDao.add(blogType);
	}

	@Override
	public Integer update(BlogType blogType) {
		return blogTypeDao.update(blogType);
	}

	@Override
	public Integer delete(Integer id) {
		return blogTypeDao.delete(id);
	}

}
