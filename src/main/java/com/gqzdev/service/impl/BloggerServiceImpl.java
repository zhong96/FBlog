package com.gqzdev.service.impl;

import com.gqzdev.dao.BloggerDao;
import com.gqzdev.entity.Blogger;
import com.gqzdev.service.BloggerService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 博主Service实现类
 * @author Administrator
 *
 */
@Service("bloggerService")
public class BloggerServiceImpl implements BloggerService{

	@Resource
	private BloggerDao bloggerDao;

	@Override
	public Blogger find() {
		return bloggerDao.find();
	}

	@Override
	public Blogger getByUserName(String userName) {
		return bloggerDao.getByUserName(userName);
	}

	@Override
	public Integer update(Blogger blogger) {
		return bloggerDao.update(blogger);
	}

}
