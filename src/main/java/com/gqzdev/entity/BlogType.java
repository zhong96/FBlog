package com.gqzdev.entity;

 /**
  * 博客类型实体
  * @param   * @param null
  * @method
  * @description:
  * @return
  * @author ganquanzhong
  * @date   2019/10/23 17:28
  */
public class BlogType {

	/**
	 *  编号
	 */
	private Integer id;

	/**
	 *  博客类型名称
	 */
	private String typeName;

	// 数量
	private Integer blogCount;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public Integer getBlogCount() {
		return blogCount;
	}
	public void setBlogCount(Integer blogCount) {
		this.blogCount = blogCount;
	}
	

	
	
}
